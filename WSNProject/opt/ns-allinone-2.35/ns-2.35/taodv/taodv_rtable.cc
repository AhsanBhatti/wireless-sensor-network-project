/*
Copyright (c) 1997, 1998 Carnegie Mellon University.  All Rights
Reserved. 

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice,
this list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright notice,
this list of conditions and the following disclaimer in the documentation
and/or other materials provided with the distribution.
3. The name of the author may not be used to endorse or promote products
derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

The TAODV code developed by the CMU/MONARCH group was optimized and tuned by Samir Das and Mahesh Marina, University of Cincinnati. The work was partially done in Sun Microsystems.
*/


#include <taodv/taodv_rtable.h>
//#include <cmu/taodv/taodv.h>

/*
  The Routing Table
*/

taodv_rt_entry::taodv_rt_entry()
{
int i;

 rt_req_timeout = 0.0;
 rt_req_cnt = 0;

 rt_dst = 0;
 rt_seqno = 0;
 rt_hops = rt_last_hop_count = INFINITY2;
 rt_nexthop = 0;
 LIST_INIT(&rt_pclist);
 rt_expire = 0.0;
 rt_flags = RTF_DOWN;

 /*
 rt_errors = 0;
 rt_error_time = 0.0;
 */


 for (i=0; i < MAX_HISTORY; i++) {
   rt_disc_latency[i] = 0.0;
 }
 hist_indx = 0;
 rt_req_last_ttl = 0;

 LIST_INIT(&rt_nblist);

}


taodv_rt_entry::~taodv_rt_entry()
{
TAODV_Neighbor *nb;

 while((nb = rt_nblist.lh_first)) {
   LIST_REMOVE(nb, nb_link);
   delete nb;
 }

TAODV_Precursor *pc;

 while((pc = rt_pclist.lh_first)) {
   LIST_REMOVE(pc, pc_link);
   delete pc;
 }

}


void
taodv_rt_entry::nb_insert(nsaddr_t id)
{
TAODV_Neighbor *nb = new TAODV_Neighbor(id);
        
 assert(nb);
 nb->nb_expire = 0;
 LIST_INSERT_HEAD(&rt_nblist, nb, nb_link);

}


TAODV_Neighbor*
taodv_rt_entry::nb_lookup(nsaddr_t id)
{
TAODV_Neighbor *nb = rt_nblist.lh_first;

 for(; nb; nb = nb->nb_link.le_next) {
   if(nb->nb_addr == id)
     break;
 }
 return nb;

}


void
taodv_rt_entry::pc_insert(nsaddr_t id)
{
	if (pc_lookup(id) == NULL) {
	TAODV_Precursor *pc = new TAODV_Precursor(id);
        
 		assert(pc);
 		LIST_INSERT_HEAD(&rt_pclist, pc, pc_link);
	}
}


TAODV_Precursor*
taodv_rt_entry::pc_lookup(nsaddr_t id)
{
TAODV_Precursor *pc = rt_pclist.lh_first;

 for(; pc; pc = pc->pc_link.le_next) {
   if(pc->pc_addr == id)
   	return pc;
 }
 return NULL;

}

void
taodv_rt_entry::pc_delete(nsaddr_t id) {
TAODV_Precursor *pc = rt_pclist.lh_first;

 for(; pc; pc = pc->pc_link.le_next) {
   if(pc->pc_addr == id) {
     LIST_REMOVE(pc,pc_link);
     delete pc;
     break;
   }
 }

}

void
taodv_rt_entry::pc_delete(void) {
TAODV_Precursor *pc;

 while((pc = rt_pclist.lh_first)) {
   LIST_REMOVE(pc, pc_link);
   delete pc;
 }
}	

bool
taodv_rt_entry::pc_empty(void) {
TAODV_Precursor *pc;

 if ((pc = rt_pclist.lh_first)) return false;
 else return true;
}	

/*
  The Routing Table
*/

taodv_rt_entry*
taodv_rtable::rt_lookup(nsaddr_t id)
{
taodv_rt_entry *rt = rthead.lh_first;

 for(; rt; rt = rt->rt_link.le_next) {
   if(rt->rt_dst == id)
     break;
 }
 return rt;

}

void
taodv_rtable::rt_delete(nsaddr_t id)
{
taodv_rt_entry *rt = rt_lookup(id);

 if(rt) {
   LIST_REMOVE(rt, rt_link);
   delete rt;
 }

}

taodv_rt_entry*
taodv_rtable::rt_add(nsaddr_t id)
{
taodv_rt_entry *rt;

 assert(rt_lookup(id) == 0);
 rt = new taodv_rt_entry;
 assert(rt);
 rt->rt_dst = id;
 LIST_INSERT_HEAD(&rthead, rt, rt_link);
 return rt;
}
////////////////////TAODV////////////////////

trust_entry::trust_entry()
{
   //Initialize as per your need.
}
trust_entry::~trust_entry()
{
  //Deconstruct as per your need.
}
// by ahsan 
trust_entry* trust_store::trust_lookup(nsaddr_t node_id)
{
     trust_entry *rp = trusthead.lh_first;
     for (; rp; rp = rp->trust_link.le_next) {
             if (rp->node_id == node_id)
                 break;
     }
    return rp;
}
//by ahsan
void trust_store::trust_delete(nsaddr_t node_id)
{
    trust_entry *rp = trust_lookup(node_id);

    if (rp)
    {
        LIST_REMOVE(rp, trust_link);
        delete rp;
    }

}
// by ahsan
trust_entry*
trust_store::trust_insert(nsaddr_t node_id,float trust_value)
{
    trust_entry *rp;
    //assert(tr_lookup(dst_seq_no) == 0);
    rp = new trust_entry;
    assert(rp);
    rp->node_id = node_id;
    rp->trust_value = trust_value;
    LIST_INSERT_HEAD(&trusthead, rp, trust_link);
    return rp;
}
//by ahsan
trust_entry* trust_store::trust_update(nsaddr_t node_id, float trust_value)
{
	trust_delete(node_id);
    trust_insert(node_id,trust_value);
    return 0;
}

packet_entry::packet_entry()
{
   //Initialize as per your need.
}
packet_entry::~packet_entry()
{
  //Deconstruct as per your need.
}
// by ahsan 
packet_entry* packet_store::packet_lookup(int packet_id)
{
     packet_entry *rp = packethead.lh_first;
     for (; rp; rp = rp->packet_link.le_next) {
             if (rp->packet_id == packet_id)
                 break;
     }
    return rp;
}
// by ahsan 
void packet_store::packet_delete(int packet_id)
{
    packet_entry *rp = packet_lookup(packet_id);
    if (rp)
    {
        LIST_REMOVE(rp, packet_link);
        delete rp;
    }
}
// by ahsan
void packet_store::packet_flush(nsaddr_t fwdr_id)
{
	 packet_entry *rp = packethead.lh_first;
	     for (; rp; rp = rp->packet_link.le_next) {
	             if (rp->fwdr_id== fwdr_id)
	             {
	            	 LIST_REMOVE(rp, packet_link);
	            	 delete rp;
	             }
	     }
}

// by ahsan
void packet_store::packet_insert(nsaddr_t source_id, nsaddr_t dest_id, nsaddr_t fwdr_id, int packet_id)
{
    packet_entry *rp;
    rp = new packet_entry;
    assert(rp);
    rp->source_id = source_id;
    rp->dest_id = dest_id;
    rp->fwdr_id = fwdr_id;
    rp->packet_id = packet_id;
    LIST_INSERT_HEAD(&packethead, rp, packet_link);
}
// by ahsan
int packet_store::packet_count(nsaddr_t fwdr_id)
{
    int count = 0;
    packet_entry *rt;

    for (rt = packethead.lh_first; rt; rt = rt->packet_link.le_next)
	{
    	if(rt->fwdr_id==fwdr_id)
	  	  count++;
	}
    return count;
}

////////////////////TAODV////////////////////
